const operators = ['+', '-', '*', '/'];
const characters = Array.from({length: 6}).map((_, index) => String.fromCodePoint(65 + index));
const api_url = 'https://allianz-evaluator.herokuapp.com';

export { api_url, characters, operators };
