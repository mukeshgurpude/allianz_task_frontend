import { useState } from 'react'
import './App.css'
import Draggable from './components/draggable'
import DropBox from './components/dropbox'
import Input from './components/right_input'
import { characters, operators } from './constants'
import { fetch_result, ICharacter } from './utils'

function App() {
  const [ expression, setExpression ] = useState<ICharacter[]>([])
  const dropHandler: React.DragEventHandler<HTMLDivElement> = function(e) {
    const current = e.dataTransfer?.getData('character')
    if (current) setExpression([...expression, { char: current, id: Date.now() }])
  }
  function addRHS(num: number) {
    // No condition before number
    if (!['<', '>'].includes(expression.at(-1)?.char as string)) {
      alert('A condition must be placed before a number')
      return
    }
    setExpression([...expression, { char: num.toString(), id: Date.now() }])
  }
  function remove_element(id: number) {
    setExpression(expression.filter(({ id: i }) => i !== id))
  }

  return <>
    {/* Operands */}
    <div className='container'> {
        characters.map((_, index) => <Draggable key={index} char={String.fromCodePoint(65 + index)} />)
    } </div>

    {/* Operators */}
    <div className='container'> {
        operators.map((char, index) => <Draggable key={index} char={char} operator />)
      } {
        ['<', '>'].map((char, index) => <Draggable key={index} char={char} operator classes='violet' />)
      }
      <Input onChange={addRHS} message='Please enter RHS' char='RHS' />
    </div>

    {/* Drop container */}
    <DropBox dropHandler={dropHandler} expression={expression} remove={remove_element} />

    <button className='btn' onClick={() => fetch_result(expression, alert)} children='Get result' />
  </>
}

export default App
