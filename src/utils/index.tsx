import { api_url } from '../constants'

export type ICharacter = { char: string, id: number }
export type IFetchResult = ( expression: ICharacter[], callback: Function ) => void

const fetch_result: IFetchResult = function( expression, callback ) {
  if (expression.length < 3) {
    alert('Expression is too short')
    return
  }
  if (Number.isNaN(Number(expression.at(-1)?.char))) {
    alert('RHS is not a number')
    return
  }
  if (!['<', '>'].includes(expression.at(-2)?.char as string)) {
    alert('A condition must be placed before a number')
    return
  }

  const data = JSON.stringify(expression.map(({ char }) => char))
  fetch(`${api_url}/calculate`, { method: 'POST', body: data, headers: { 'Content-Type': 'application/json' }})
    .then(res => res.json())
    .then(res => callback(res))
    .catch(err => callback(err))
}

export { fetch_result }
