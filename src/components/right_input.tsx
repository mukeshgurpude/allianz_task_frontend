type Props = { char: string, message: string, onChange: (ans: number) => void }


function get_input(func: (message: number) => void, message: string) {
  return function(e: React.MouseEvent<HTMLDivElement>) {
    const answer = prompt(message)
    if (answer && answer.length > 0 && Number.isInteger(Number(answer))) {
      func(Number(answer))
    } else {
      alert('Please enter a valid number')
    }
  }
}

export default function Input({ char, message, onChange }: Props) {
  return <div className="box" data-bg='violet' onDoubleClick={get_input(onChange, message)} children={char} />
}
