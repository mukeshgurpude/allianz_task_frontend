import { DragEventHandler } from "react"

type Props = { char: string, children?: React.ReactElement, classes?: string, no_events?: boolean, operator?: boolean }

function setDragData(char: string) {
  const handler: DragEventHandler<HTMLDivElement> = function(e) {
    e.dataTransfer?.setData('character', char.toLowerCase())
  }
  return handler
}


export default function Draggable({ char, classes = '', no_events=false, operator=false, children }: Props) {
  return <div {...(no_events ? {} : { onDragStart: setDragData(char), draggable: true })}
    className={`box ${classes} ${operator && 'operator'}`}>
      <p>{char}</p>
      {children}
  </div>
}
