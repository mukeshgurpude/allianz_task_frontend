import { operators } from "../constants"
import { ICharacter } from "../utils"
import Draggable from "./draggable"

type Props = { dropHandler: React.DragEventHandler, expression: ICharacter[], remove: Function }


export default function DropBox({ dropHandler, expression, remove }: Props) {
  return <div className="container drop-box" onDrop={dropHandler} onDragOver={(e) => e.preventDefault()}>{
    expression.map(({ char, id }) =>  <Draggable key={id} no_events char={char} operator={operators.includes(char)} children={
      <button onClick={() => remove(id)}>X</button>
    } />)
  }</div>
}
